let Room = {
  init (socket, el) {
    let roomId = el.getAttribute('data-id')
    console.log(`element: ${el}, roomId: ${roomId}`)
    socket.connect()
    this.onReady(roomId, socket)
  },
  onReady (roomId, socket) {
    let msgInput = document.getElementById('msg-input')
    let postButton = document.getElementById('msg-submit')
    let msgContainer = document.getElementById('msg-container')
    let roomChannel = socket.channel('rooms:' + roomId)
    roomChannel.on('new_chat', resp => {
      console.log(`NEW CHAT: ${resp['body']}`)
      this.renderChat(msgContainer, resp)
    })
    roomChannel
      .join()
      .receive('ok', resp => console.log('joined room channel', resp))
      .receive('error', reason =>
        console.error('failed to join room channel', reason)
      )
    postButton.addEventListener('click', e => {
      roomChannel
        .push('new_chat', { body: msgInput.value })
        .receive('error', e => console.log(e))
      msgInput.value = ''
    })
  },
  renderChat (msgContainer, { body }) {
    console.log(`rendering ${body}`)
    let div = document.createElement('div')
    msgContainer.appendChild(div)
    div.innerText = `Anonymous: ${body}`
    msgContainer.scrollTop = msgContainer.scrollHeight
  }
}

export default Room
